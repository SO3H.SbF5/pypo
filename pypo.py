import urllib3
import requests
import bs4
import random as rd

proxies = {
  'http': 'http://172.16.0.252:3128',
  'https': 'http://172.16.0.252:3128',
}

def wiki(n, art=0):
    """Return n sentences from n Wikipedia's articles"""
    assert type(n)==int, 'n must be an integer'
    assert n >= 2, 'n must be higher than 0'
    assert n <= 20, 'n must be lower than 20'
    
    text = ''
    articles = []
    
    while len(articles)<=n-1:
        r = requests.get('https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard', proxies=proxies)
        
        soup = bs4.BeautifulSoup(r.text, 'html.parser')
        
        content = soup.find("div", {"class": "mw-parser-output"})
        content = content.findAll('p')
    
        if len(content) >= 5:
            txt = content[4].text

            #print(list(txt))
            
            if len(list(txt)) > 20:
                
                if txt.count("Wiki")==0 and txt.count("wiki")==0:
                    splitted = txt.split(".")
                    
                    if len(articles) == 0: sep = ""
                    else: sep = ". "
                    
                    t = splitted[rd.randint(0, len(splitted)-1)]
                    if t != " " and  t != "" and t.count("Géolocalisation") == 0:
                        text = text + sep + t.replace('/(\[\d\])/', '')
                        articles.append(soup.title.text.replace(u' — Wikipédia', u''))
    
    text = text + "."
    text = text.replace(u'\xa0', u' ')
    
    if art == 1 : return (text, articles)
    else: return text